import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';


import {AppComponent} from './app.component';
import {ChannelsTopVideosService} from './services/channels-top-videos.service';
import {LikesFilterPipe} from './filters/likes-filter.pipe';
import { FilterPipe } from './filters/filter.pipe';


@NgModule({
  declarations: [
    AppComponent,
    LikesFilterPipe,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [ChannelsTopVideosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
