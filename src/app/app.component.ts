import {Component, OnInit} from '@angular/core';
import {ChannelsTopVideosService} from './services/channels-top-videos.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  data: any;
  perPage = 10;
  search = '';
  selectedPage = 0;
  shouldFilterLikes = false;
  pagination: object = {
    '10': [0, 1, 2, 3, 4],
    '25': [0, 1],
    '50': [0]
  };

  constructor(private channelService: ChannelsTopVideosService) {
  }

  ngOnInit() {
    this.data = this.getMoviesData();
  }

  getMoviesData() {
    return this.channelService.getData();
  }
}
