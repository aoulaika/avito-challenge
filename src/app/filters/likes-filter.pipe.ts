import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'likesFilter'
})
export class LikesFilterPipe implements PipeTransform {

  transform(movies: object[], active = false, like: number = 10): any {
    if (movies && active) {
      return movies.filter(movie => movie['user']['metadata']['connections']['likes']['total'] > like);
    }
    return movies;
  }

}
