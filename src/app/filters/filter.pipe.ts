import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(movies: object[], property: string = 'description', value: any): any {
    if (movies && value) {
      return movies.filter(movie => movie[property] && movie[property].includes(value));
    }
    return movies;
  }

}
