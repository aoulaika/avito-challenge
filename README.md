#Ayoub Oulaika
### Avito Front End Developer Challenge

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.0.

### Development server
Run `yarn install` or `npm install` first


Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.


### Why Angular & [Angular CLI](https://github.com/angular/angular-cli)

Since I had more experience working with angular I choose the framework to easilly complete the challenge in the specific deadline.

I've also used Angular CLI to create my solution so I can quickly generate project

### What can I improve & Fix

* Fix number of plays (if the correct value is the one under ``stats plays`` then it's corrupted from the data I got from the API and no need to fix it on the view)
* Create a separate component for Card element, Nav Bar and Side Bar
* Provide the ability to see all video description when clicking the 3 dots at the end
* Create a directive to shorten the movie description
* Fix responsive design

### Environment

``node v8.6.0``
``yarn 1.2.1``

Tested on `Chrome Version 62.0.3202.89 (Official Build) (64-bit)`
